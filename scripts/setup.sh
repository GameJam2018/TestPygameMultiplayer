
# relying on the fact that most package managers basically operate in the same way here...
${PACKAGE_MANAGER} update
${PACKAGE_MANAGER} install python3
${PACKAGE_MANAGER} install pip3 

pip3 install -r Client/requirements.txt
pip3 install -r Server/requirements.txt