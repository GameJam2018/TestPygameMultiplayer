"""
Acknowledgments: 

* Example tornado client/server code from https://github.com/mivade/tornado-tcp-echo-demo/blob/master/echo.py

"""

from tornado.ioloop import IOLoop
from tornado.options import define, options
from tornado import gen
from tornado.queues import Queue
from tornado.iostream import StreamClosedError
from tornado.tcpclient import TCPClient
from tornado.tcpserver import TCPServer
import json
import string
import random

define('port', default=8080, help="TCP port to use")
define('server', default=False, help="Run as the echo server")
define('encoding', default='utf-8', help="String encoding")

MESSAGE_TYPE_NOTIFY = 'n'
MESSAGE_TYPE_JOIN = 'nj'
MESSAGE_TYPE_JOIN_SUCCESS = 'jt'
MESSAGE_TYPE_JOIN_FAIL = 'jf'
MESSAGE_TYPE_INIT = 'i'
MESSAGE_TYPE_LEAVE = 'nl'

class EchoServer(TCPServer):
    """Tornado asynchronous echo TCP server."""
    clients = set()
    
    group = None
    client = None

    # { 'groupCode' : [stream, stream] }
    groups = {}
    
    @gen.coroutine
    def handle_stream(self, stream, address):
        ip, fileno = address
        print("Incoming connection from " + ip)
        EchoServer.clients.add(address)
        while True:
            try:
                yield self.route(stream)
            except StreamClosedError:
                print("Client " + str(address) + " left.")
                EchoServer.clients.remove(address)

                if EchoServer.groups.get(self.group) != None:
                    EchoServer.groups[self.group].remove(stream)

                if (len(EchoServer.groups.get(self.group)) == 0):
                    print ('Group ' + str(self.group) + ' empty. Closing group.')
                    EchoServer.groups.pop(self.group)
                else:
                    # inform all members of group of departed client
                    for stream in self.groups.get(self.group):
                        response = {'type': MESSAGE_TYPE_LEAVE, 'group': self.group, 'client': self.client}
                        responseStr = json.dumps(response)
                        yield stream.write((responseStr + chr(3)).encode(options.encoding))
                break

    @gen.coroutine
    def route(self, stream):
        byteData = yield stream.read_until(chr(3).encode(options.encoding))
        strData = byteData.decode(options.encoding)

        if (ord(strData[len(strData) - 1]) == 3):
            strData = strData[:-1]
        
        data = json.loads(strData) 

        print(data)

        if data.get('type') == MESSAGE_TYPE_NOTIFY:
            yield self.forward(stream, strData)
        elif data.get('type') == MESSAGE_TYPE_INIT:
            yield self.initGroup(stream)
        elif data.get('type') == MESSAGE_TYPE_JOIN:
            yield self.joinGroup(stream, data)

        # strData = strData.split(chr(3))[0]
        # print('Echoing data: ' + strData)
        # yield stream.write(data + chr(3).encode(options.encoding))

    @gen.coroutine
    def forward(self, stream, strData):
        if self.group != None:
            print('forwarding to group... ' + strData)
            streams = self.groups[self.group]
            print(streams)
            for stream in streams:
                print (stream)
                yield stream.write((strData + chr(3)).encode(options.encoding))
        else:
            yield self.stream.write((strData + chr(3)).encode(options.encoding))

    @gen.coroutine
    def initGroup(self, stream):
        self.group = ''.join(random.choices(string.ascii_uppercase + string.digits, k=8))
        self.client = ''.join(random.choices(string.ascii_uppercase + string.digits, k=8))
        EchoServer.groups[self.group] = []
        EchoServer.groups[self.group].append(stream)

        response = {'type': MESSAGE_TYPE_JOIN_SUCCESS, 'group': self.group, 'client': self.client}
        responseStr = json.dumps(response)
        yield stream.write((responseStr + chr(3)).encode(options.encoding))

    @gen.coroutine
    def joinGroup(self, stream, data):
        # If it's joining an existing group
        print(data)
        if EchoServer.groups.get(data.get('group')) != None:
            self.group = data.get('group')
            self.client = ''.join(random.choices(string.ascii_uppercase + string.digits, k=8))
            print('joining group pre: ' + str(EchoServer.groups[self.group]))
            EchoServer.groups[self.group].append(stream)
            print('joining group post: ' + str(EchoServer.groups[self.group]))

            # inform new client of its client id
            response = {'type': MESSAGE_TYPE_JOIN_SUCCESS, 'group': self.group, 'client': self.client}
            responseStr = json.dumps(response)
            yield stream.write((responseStr + chr(3)).encode(options.encoding))

            # inform current member and everyone in group of new client
            for stream in self.groups.get(self.group):
                response = {'type': MESSAGE_TYPE_JOIN, 'group': self.group, 'client': self.client}
                responseStr = json.dumps(response)
                yield stream.write((responseStr + chr(3)).encode(options.encoding))
        else:
            response = {'type': MESSAGE_TYPE_JOIN_FAIL, 'group': None, 'newClientID': None}
            responseStr = json.dumps(response)
            yield stream.write((responseStr + chr(3)).encode(options.encoding))

def start_server():
    server = EchoServer()
    server.listen(options.port)
    print("Starting server on tcp://localhost:" + str(options.port))
    IOLoop.instance().start()

if __name__ == "__main__":
    start_server()
 