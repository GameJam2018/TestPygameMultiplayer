# Test PyGame Multiplayer
===============

This is a simple demo/trial run of making a live multiplayer pygame based game.

Some important things to get started:
* We're using python3 for this
* If you're on mac/linux, you can use the setup scripts to get going (sorry, no such hope if you're on windows.)

## Setup (Mac/Linux):
1. Modify the scripts/env.sh file for your system
2. Set up your environment variables 
    $ source ./scripts/env.sh
3. Install everything necessary: 
    $ ./setup.sh

## Developing (mac/linux):

To run the client, use the ./start_client.sh script 
To run the server locally, use the start_server_local.sh script

## How it Works (end goal)

The end goal here is for an easy way to create a fast multiplayer experience. Multiple clients should be able to connect to a server, and any messages they send to the server will be streamed out to all other clients participating in their game. The server will hold *no* state (well as little as possible... :/)

Each client will be responsible for moving their player (yes, we're placing absolute trust in the client... I know, that allows for cheating but its a hackathon so who cares.) Specific decisions, such as whether a player hit another player, may need to be decided via raft consensus accoss all clients (if a majority agree it was a hit, the server sends out a correct response back - note to self: we're going to have to add some logic to the server for this, and unfortanatley potentialy some state).

My thoughts are that one client will request a code from the server for a new game instance, which can be handed out to all the other clients.