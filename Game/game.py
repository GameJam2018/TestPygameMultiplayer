import sys
import pygame
from pygame.locals import *
from tornado.options import define, options

from MultiplayerGame.MultiplayerGame import MultiplayerGame

define('port', default=8080, help="TCP port to use")
define('ip', default='127.0.0.1', help="Web address of client to use")
define('encoding', default='utf-8', help="String encoding")
define('master', default=False, help="Should this game launch a new multiplayer server instance")
define('groupCode', default=None, help="The multiplayer group to connect to")

# set up the colors
BLACK = (  0,   0,   0)
WHITE = (255, 255, 255)
RED   = (255,   0,   0)
GREEN = (  0, 255,   0)
BLUE  = (  0,   0, 255)

class Game(MultiplayerGame):

    def __init__(self):
        MultiplayerGame.__init__(self)

        # lets set up the connection to the game server
        if (options.master):
            groupClient = self.dataStream.initGroup()
        elif (options.groupCode != None):
            groupClient = self.dataStream.joinGroup(options.groupCode)
            print('join group complete')
            print (groupClient)

    # setup is called once, and can be used to create sprites etc.
    def setup(self):
        pass

    # update is called repeatedly on loop
    def update(self):

        while self.dataStream.messagesReady():
            newObj = self.dataStream.receive()
            if newObj.get('client') != self.dataStream.getClientID():
                print (newObj)

            # parse the object here
            # print(newObj)

        self.dataStream.send({'message': 'world'})

        # draw on the surface object
        self.display.fill(WHITE)
        pygame.draw.polygon(self.display, GREEN, ((146, 0), (291, 106), (236, 277), (56, 277), (0, 106)))
        pygame.draw.line(self.display, BLUE, (60, 60), (120, 60), 4)
        pygame.draw.line(self.display, BLUE, (120, 60), (60, 120))
        pygame.draw.line(self.display, BLUE, (60, 120), (120, 120), 4)
        pygame.draw.circle(self.display, BLUE, (300, 50), 20, 0)
        pygame.draw.ellipse(self.display, RED, (300, 200, 40, 80), 1)
        pygame.draw.rect(self.display, RED, (200, 150, 100, 50))
        
        pixObj = pygame.PixelArray(self.display)
        pixObj[380][280] = BLACK
        pixObj[382][282] = BLACK
        pixObj[384][284] = BLACK
        pixObj[386][286] = BLACK
        pixObj[388][288] = BLACK
        del pixObj

game = Game()
game.start()