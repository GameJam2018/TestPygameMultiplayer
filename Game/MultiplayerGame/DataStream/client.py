from tornado.ioloop import IOLoop
from tornado.options import define, options
from tornado import gen
from tornado.iostream import StreamClosedError
from tornado.tcpclient import TCPClient
from tornado.tcpserver import TCPServer
import json

def client(sendQueue, receiveQueue):

    @gen.coroutine
    def send(stream, text):
        if not isinstance(text, str):
            text = json.dumps(text)
        """Send the text to the server and print the reply."""
        if text[-1] != chr(3):
            text = text + chr(3)
        #print('sent: ' + repr(text))
        yield stream.write(text.encode(options.encoding))

    @gen.coroutine
    def receive(stream):
        data = yield stream.read_until(chr(3).encode(options.encoding))
        decodedData = data.decode(options.encoding).split(chr(3))[0]

        if (len(decodedData) > 0):
            receiveQueue.put(decodedData)
             
        

    @gen.coroutine
    def run_client():
        """Setup the connection to the echo server and wait for user
        input.
        """
        stream = yield TCPClient().connect(options.ip, options.port)
        try:
            while True:
                if not sendQueue.empty():
                    yield send(stream, sendQueue.get())
                    yield receive(stream)
        except KeyboardInterrupt:
            stream.close()

    IOLoop.instance().run_sync(run_client)