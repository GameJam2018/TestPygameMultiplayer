import MultiplayerGame.DataStream.client as client
from multiprocessing import Process, Queue
import json

class DataStream:

    MESSAGE_TYPE_NOTIFY = 'n'
    MESSAGE_TYPE_JOIN = 'nj'
    MESSAGE_TYPE_JOIN_SUCCESS = 'jt'
    MESSAGE_TYPE_JOIN_FAIL = 'jf'
    MESSAGE_TYPE_INIT = 'i'
    MESSAGE_TYPE_LEAVE = 'nl'
    
    def __init__(self):
        self.sendQueue = Queue()
        self.controlQueue = Queue()
        self.notifyQueue = Queue()
        self._receiveQueue = Queue()
        self.groupCode = ""
        self.clientID = ""

        self.streamProcess = Process(target=client.client, args=(self.sendQueue, self._receiveQueue, ))
        self.streamProcess.start()

        self.routerProcess = self.routeProcess = Process(target=self._routeMessages, args=(self._receiveQueue, self.controlQueue, self.notifyQueue,))
        self.routeProcess.start()

    def connected(self):
        return (self.groupCode != None) and (self.clientID != None)

    def getGroupCode(self):
        return self.groupCode

    def getClientID(self):
        return self.clientID

    def initGroup(self):
        finalObj = {'type': self.MESSAGE_TYPE_INIT}
        message = json.dumps(finalObj)
        self.sendQueue.put(message)

        gotGroup = False

        while not gotGroup:
            if self._controlMessagesReady():
                message = self._receiveControl()
                if (message.get("type") == self.MESSAGE_TYPE_JOIN_SUCCESS):
                    self.groupCode = message.get('group')
                    self.clientID = message.get('client')
                    gotGroup = True

                    print('group code: ' + self.groupCode)
                    print('client id: ' + self.clientID)

                    return (self.groupCode, self.clientID)
                elif (message.get("type") == self.MESSAGE_TYPE_JOIN_FAIL):
                    return None

    def joinGroup(self, groupCode):
        self.groupCode = groupCode
        finalObj = {'type': self.MESSAGE_TYPE_JOIN, 'group': self.groupCode}
        message = json.dumps(finalObj)
        self.sendQueue.put(message)

        joinedGroup = False

        while not joinedGroup:
            if self._controlMessagesReady():
                message = self._receiveControl()
                if (message.get('type') == self.MESSAGE_TYPE_JOIN_SUCCESS):
                    self.clientID = message.get('client')
                    return (self.groupCode, self.clientID)
                elif (message.get("type") == self.MESSAGE_TYPE_JOIN_FAIL):
                    return None

    def _routeMessages(self, receiveQueue, controlQueue, notifyQueue):
        while True:
            if not receiveQueue.empty():
                message = receiveQueue.get()
                objMessage = json.loads(message)
                if objMessage.get('type') == self.MESSAGE_TYPE_NOTIFY:
                    
                    notifyQueue.put(objMessage)
                else:
                    controlQueue.put(objMessage)
         
    def send(self, obj, messageType=MESSAGE_TYPE_NOTIFY):
        finalObj = {'type': messageType, 'group' : self.groupCode, 'client': self.clientID, 'payload': obj}
        message = json.dumps(finalObj)
        self.sendQueue.put(message)

    def _controlMessagesReady(self):
        return not self.controlQueue.empty()

    def messagesReady(self):
        return not self.notifyQueue.empty()

    def _receiveControl(self):
        try:
            if self._controlMessagesReady():
                return self.controlQueue.get()
            else:
                return None
        except:
            return None

    def receive(self):
        try:
            if self.messagesReady():
                return self.notifyQueue.get()
            else:
                return None
        except:
            return None

        
    def closeConnection(self):
        self.streamProcess.join()
        self.routeProcess.join()