from MultiplayerGame.DataStream.DataStream import DataStream 
import pygame as pygame
import sys
from pygame.locals import *
from tornado.options import define, options

class MultiplayerGame:

    def __init__(self):
        options.parse_command_line()

        print (options.master)
        self.dataStream = DataStream()
        self._initPygame()
    
    """
    _initPygame initialises pygame. Extend this if you wish to modify the initialisation process/
    """
    def _initPygame(self):
        pygame.init()

        self.display = self._initDisplay()

    """
    _initDisplay initialises a pygame display and returns it. Extend this if you wish to modify the creation 
    of the display from a default
    """
    def _initDisplay(self):
        display = pygame.display.set_mode((400, 300), 0, 32)
        pygame.display.set_caption('Drawing')
        return display

    """
    start starts the game
    """
    def start(self):
        self.setup()

        while True:
            for event in pygame.event.get():
                if event.type == QUIT:
                    pygame.quit()
                    serverClientProc.join()
                    sys.exit()

            self.update()

            pygame.display.update()

    """
    setup is run once, and can be used to create sprites, load elements etc.
    """
    def setup(self):
        pass

    """
    update is run once on each update loop, and can be used to update elements.
    display draw is called for you after this function returns
    """
    def update(self):
        pass